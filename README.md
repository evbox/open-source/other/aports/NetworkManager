# NetworkManager

This repository is a copy of the Alpine 3.12 NetworkManager package
(https://git.alpinelinux.org/aports.git) sources.
It exsist to allow for additional patches.

The additional patches fix the below issues:
https://bugzilla.redhat.com/show_bug.cgi?id=1781253#c8
https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/issues/597
https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/issues/656
