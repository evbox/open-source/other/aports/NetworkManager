#
# Copyright (C) 2020 EVBox Intelligence B.V.
#

stages:
  - package
  - deploy


# Common parameters
# =============================================================================
default:
  tags:
    - docker

.artifact_paths: &artifact_paths
  paths:
    - "output/**/*.apk"

.git_login: &git_login
  - git config --global user.name "${BOT_USER_NAME}"
  - git config --global user.email "${BOT_USER_MAIL}"
  - git config --global credential.helper "cache --timeout=2147483647"
  - |
    _git_credentials="url=${CI_SERVER_URL}"
    _git_credentials="${_git_credentials}\nusername=${BOT_USER_NAME}"
    _git_credentials="${_git_credentials}\npassword=${CI_PERSONAL_TOKEN}\n\n"
    printf "${_git_credentials}" | git credential approve

.git_logout: &git_logout
  - git credential-cache exit


# Git authentication
# ===========================================================================
.git:
  image: "registry.hub.docker.com/gitscm/git:latest"
  before_script:
    - *git_login
  after_script:
    - *git_logout
  variables:
    GIT_DEPTH: "0"


# Build alpine package
# tag v1.4 is used for a build against Alpine 3.12
# =============================================================================
.packaging:
  image: "registry.gitlab.com/esbs/package_builder-alpine/${TARGET_ARCH}/package_builder-alpine:v1.4"
  stage: package
  artifacts:
    <<: *artifact_paths
    expire_in: "1 month"
  before_script:
    - cp "${CI_SIGNING_KEY}" "/run/${CI_SIGNING_KEY_NAME}"
    - cp "${CI_SIGNING_KEY_PUB}" "/run/${CI_SIGNING_KEY_NAME}.pub"
  script:
    - package_builder-alpine.sh -k "/run/${CI_SIGNING_KEY_NAME}"
  after_script:
    - rm -rf "/run/${CI_SIGNING_KEY_NAME}"
    - rm -rf "/run/${CI_SIGNING_KEY_NAME}.pub"

package_arm32v7:
  extends: .packaging
  variables:
    TARGET_ARCH: "arm32v7"
  tags:
    - qemu

package_amd64:
  extends: .packaging
  variables:
    TARGET_ARCH: "amd64"


# Deploy packages
# =============================================================================
.package_deploy_script: &package_deploy_script
  - |
    find "${CI_PROJECT_DIR}/output/" -iname '*.apk' | while read -r apk; do
      apk_path="$(dirname ${apk})"
      arch="${apk_path##*/}"
      target_dir="./internal/${arch}"
      mkdir -p "${target_dir}"
      cp "${apk}" "${target_dir}"
      git add "${target_dir}/${apk##*/}"
    done
  - added_packages="$(git diff --name-only --cached)"
  - |
    git commit \
      --message "Add package(s) ${CI_PROJECT_NAME}-${CI_COMMIT_TAG:-${CI_COMMIT_BRANCH}-g${CI_COMMIT_SHORT_SHA}}" \
      --message "From pipeline job: ${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}" \
      --message "Add:" \
      --message "${added_packages}" \
      --message "$(date -Iseconds)"
  - git pull --rebase
  - git push

.package_clone_script: &package_clone_script
  - git clone --depth=1
              --no-single-branch
              "https://${PACKAGES_REPOSITORY_URL}"
              "./packages"
  - cd "./packages"

.package_deploy:
  extends: .git
  stage: deploy
  variables:
    GIT_STRATEGY: none

package_deploy_release:
  extends: .package_deploy
  script:
    - *package_clone_script
    - git checkout "release/latest"
    - *package_deploy_script
  only:
    - /^v\d+(\.\d+){2}(-rc\d+)?(-r\d+)?$/

package_deploy_master:
  extends: .package_deploy
  script:
    - *package_clone_script
    - *package_deploy_script
  only:
    - master

package_deploy_dev:
  extends: .package_deploy
  script:
    - *package_clone_script
    - git checkout "${EVBOX_REPO_DEPLOY_TO:-development}"
    - *package_deploy_script
  except:
    - master
    - /^v\d+(\.\d+){2}(-rc\d+)?$/
